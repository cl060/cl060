#include <stdio.h>
#include<math.h>
int input()
{
	float a; 
	printf("Enter the time in hours\n");
	scanf("%f",&a);
	return a;
}
int convert_hrs_into_mins(float hrs)
{
	float total_mins;
	total_mins = hrs*60;
	return total_mins;
}

void output(float total_mins)
{
	printf("Total minutes converted from hours is = %0.2f\n",total_mins);
}

int main()
{
	float x,y;
	x=input();
	y=convert_hrs_into_mins(x);
	output(y);
	return 0;
}
