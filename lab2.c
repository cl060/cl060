#include <stdio.h>
#include<math.h>
#define PI 3.14
int input()
{
	float a; 
	printf("Enter the radius of the circle\n");
	scanf("%f",&a);
	return a;
}

int area_of_circle(float r)
{
	float area;
	area = PI*r*r;
	return area;
}

void output(float area)
{
	printf("Area of circle is %0.2f\n",area);
}

int main()
{
	float x,y;
	x=input();
	y=area_of_circle(x);
	output(y);
	return 0;
}
